package Tema1ISP;

import java.util.Scanner;

public class Lab3Ex1 
{
	public static void main(String[] args)
	{
		System.out.println("Introduceti valoarea dorita: ");
		Scanner tin = new Scanner (System.in);
		int k = tin.nextInt();
		Robot r1 = new Robot(1);
		r1.Change(k);
		String x = r1.toString();
		System.out.println(x);
		tin.close();
	}
}

class Robot 
{
	int x;
	
	Robot(int x) 
	{
		this.x = x;
	}
	
	Robot Change (int k) 
	{
		if(k>=1)
			x += k;
		return this;
	}
	
	public String toString() 
	{
		return "Pozitia este: "+x;
	}
}