import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.util.*;

public class Ex2 extends JFrame
{
    JTextField newItemField;
    JButton addButton;

    Ex2()
    {
        setTitle("Counting");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,200);
        setVisible(true);
    }

    public void init() 
    {
        this.setLayout(null);
        int width = 100;
        int height = 30;
        addButton = new JButton("Add");
        addButton.setBounds(50, 100, width, height);
        newItemField = new JTextField("0");
        newItemField.setBounds(50, 50, width, height);

        addButton.addActionListener(new ButtonAdd());

        add(addButton);
        add(newItemField);
    }

    public static void main(String[] args) 
    {
        new Ex2();
    }

    class ButtonAdd implements ActionListener 
    {
        int i = 0;
        public void actionPerformed(ActionEvent e)
        {
            i++;
            Ex2.this.newItemField.setText(Integer.toString(i));
        }
    }

}


